<?php

// Configurações de conexão com o banco de dados
$host = "127.0.0.1";
$usuario = "dominik";
$senha = "BD57JdY97vkctRB!@";
$banco = "dominik";

// Conecta ao banco de dados
$mysqli = new mysqli($host, $usuario, $senha, $banco);

// Verifica a conexão
if ($mysqli->connect_error) {
    die("Erro na conexão com o banco de dados: " . $mysqli->connect_error);
}

// Consulta o valor atual no banco de dados
$query = "SELECT value FROM core_config_data WHERE path = 'catalog/search/elasticsearch7_server_port'";
$result = $mysqli->query($query);

if ($result) {
    // Obtém a porta do banco de dados
    $row = $result->fetch_assoc();
    $db_port = $row['value'];

    // Obtém a porta do contêiner usando o script Bash
    $bash_port = shell_exec('./get_port.sh');

    // Compara as portas
    if ($db_port != $bash_port) {
        // Atualiza a porta no banco de dados
        $update_query = "UPDATE core_config_data SET value = '$bash_port' WHERE path = 'catalog/search/elasticsearch7_server_port'";
        $mysqli->query($update_query);

        echo "A porta no banco de dados foi atualizada para $bash_port.";
    } else {
        echo "A porta no banco de dados já está atualizada.";
    }

    // Libera o resultado da consulta
    $result->free();
} else {
    echo "Erro na consulta: " . $mysqli->error;
}

// Fecha a conexão com o banco de dados
$mysqli->close();

?>

