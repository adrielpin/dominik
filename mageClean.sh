#!/bin/bash
composer install && bin/magento s:up && bin/magento s:di:compile && bin/magento s:static-content:deploy en_US pt_BR -f -j 8 && bin/magento c:c && bin/magento c:f && bin/magento indexer:reindex
