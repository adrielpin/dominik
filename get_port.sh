#!/bin/bash

# Nome do contêiner Docker
container_name="elasticsearch"

# Verifica se o contêiner está em execução
if [ "$(docker inspect -f '{{.State.Running}}' $container_name 2>/dev/null)" == "true" ]; then
    # Obtém a porta do contêiner se estiver em execução
    port=$(docker inspect --format='{{(index (index .NetworkSettings.Ports "9200/tcp") 0).HostPort}}' $container_name)

    echo "$port"
else
    echo "0"
fi

