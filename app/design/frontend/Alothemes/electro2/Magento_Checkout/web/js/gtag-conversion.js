define([
    'jquery'
], function ($, _) {
    'use strict';

    $.widget('mage.gtagConversion',{

        _create: function() {
            window.gtag('event', 'purchase', $.parseJSON(this.options.payload));
        }
    });

    return $.mage.gtagConversion;
});
