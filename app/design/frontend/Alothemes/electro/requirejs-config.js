/*
* @Author: nguyen
* @Date:   2019-06-03 14:48:52
* @Last Modified by:   nguyen
* @Last Modified time: 2020-06-30 20:31:19
*/
var config = {
    config: {
        mixins: {
            'Magento_ReCaptchaFrontendUi/js/reCaptcha': {
                'js/mixins/reCaptcha-mixin': true
            }
        }
    },
};



