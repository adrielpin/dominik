function copyQrCode() {
    const inputElement = document.querySelector('#mp_qr_code');
    if (inputElement) {
        inputElement.select();
        inputElement.setSelectionRange(0, 99999); 
        document.execCommand('copy');
        console.log('Valor copiado: ' + inputElement.value);
    } else {
        console.log('Input não encontrado');
    }
}

var $_GET = new Array();
	function GET() {
		var url = location.search.replace("?", "").split("&");
		for (var index = 0; index < url.length; index++) {
			var value = url[index].split("=");
			$_GET[value[0]] = value[1];
		}

	}
GET();

function enviaConteudoParaAPI(cep){
			if (cep.length<8){
				return false;
			}
			var content, retorno;
			var urlEnvio = 'https://viacep.com.br/ws/'+cep+'/json/';
			jQuery.ajax({
				type: 'GET',
				timeout: 10000,
				async: false,
				url: urlEnvio,
			}).then(function(response){
				retorno = response;

				}, function(response){
					retorno = response;

				});
jQuery('input#street_1,[name="street[0]"]').val(retorno.logradouro);			
			jQuery('input#street_3,[name="street[2]"]').val(retorno.bairro);
			jQuery('input#city,[name="city"]').val(retorno.localidade);

			switch (retorno.uf) {
				  case 'AC': jQuery('#region_id,[name=region_id]').val('485'); break;
				  case 'AL': jQuery('#region_id,[name=region_id]').val('486'); break;
				  case 'AP': jQuery('#region_id,[name=region_id]').val('487'); break;
				  case 'AM': jQuery('#region_id,[name=region_id]').val('488'); break;
				  case 'BA': jQuery('#region_id,[name=region_id]').val('489'); break;
				  case 'CE': jQuery('#region_id,[name=region_id]').val('490'); break;
				  case 'DF': jQuery('#region_id,[name=region_id]').val('511'); break;
				  case 'ES': jQuery('#region_id,[name=region_id]').val('491'); break;
				  case 'GO': jQuery('#region_id,[name=region_id]').val('492'); break;
				  case 'MA': jQuery('#region_id,[name=region_id]').val('493'); break;
				  case 'MT': jQuery('#region_id,[name=region_id]').val('494'); break;
				  case 'MS': jQuery('#region_id,[name=region_id]').val('495'); break;
				  case 'MG': jQuery('#region_id,[name=region_id]').val('496'); break;
				  case 'PR': jQuery('#region_id,[name=region_id]').val('499'); break;
				  case 'PB': jQuery('#region_id,[name=region_id]').val('498'); break;
				  case 'PA': jQuery('#region_id,[name=region_id]').val('497'); break;
				  case 'PE': jQuery('#region_id,[name=region_id]').val('500'); break;
				  case 'PI': jQuery('#region_id,[name=region_id]').val('501'); break;
				  case 'RN': jQuery('#region_id,[name=region_id]').val('503'); break;
				  case 'RS': jQuery('#region_id,[name=region_id]').val('504'); break;
				  case 'RJ': jQuery('#region_id,[name=region_id]').val('502'); break;
				  case 'RO': jQuery('#region_id,[name=region_id]').val('505'); break;
				  case 'RR': jQuery('#region_id,[name=region_id]').val('506'); break;
				  case 'SC': jQuery('#region_id,[name=region_id]').val('507'); break;
				  case 'SE': jQuery('#region_id,[name=region_id]').val('509'); break;
				  case 'SP': jQuery('#region_id,[name=region_id]').val('508'); break;
				  case 'TO': jQuery('#region_id,[name=region_id]').val('510'); break;				  
			}
                        jQuery('#region_id,[name=region_id] ,[name="street[0]"], [name=city]').change();
			return retorno;
		}

if (typeof $_GET['key_uri']!='undefined'  && (window.location.pathname == '/checkout/' || window.location.pathname == '/customer/address/new/' || window.location.pathname.includes('/customer/address/edit/')   )) {
	function tratamento_cadastro(){

jQuery('body').prepend("<style>.campo-errado{    border: red 1px solid !important;}.opcao-pf, .opcao-pj {    background-color: #fed900;    padding: 0 10px;    border-radius: 8px;	cursor: pointer;}.opcao-pf:hover, .opcao-pj:hover {    -border: blue 1px  inset;    background-color: #d3b501;}.opcao-pf.clicked, .opcao-pj.clicked {    border: blue 1px  inset;    background-color: #d3b501;}/*[name=vat_id]{	display:none;}*/.forca-display-block{	display:block !important;margin-top: 5px;}.add-after-missing:before{	content: 'Existem campos que precisam ser preenchidos acima.';}.field.choice.set.billing,.field.choice.set.shipping{order:10}#shipping-method-buttons-container button.button.action.continue.primary{float:right}</style>");
		jQuery('[name=vat_id]').attr('placeholder','Selecione uma opção acima');
		jQuery('[name="shippingAddress.vat_id"] label span, [name="shippingAddress.postcode"] label span').append(' *');		

		if (jQuery('#opc-new-shipping-address [name="street[3]"],[name="street[3]"],#street_4').val()==''){
			jQuery('#opc-new-shipping-address [name="street[3]"],[name="street[3]"],#street_4').val('(vazio)');
		}
		jQuery(document).on('blur','#opc-new-shipping-address [name="street[3]"],[name="street[3]"],#street_4',function(){
			jQuery(this).val(jQuery(this).val().trim());
			if (jQuery(this).val()==''){
				jQuery(this).val('(vazio)');
			}
		});
		jQuery(document).on('focus','#opc-new-shipping-address [name="street[3]"],[name="street[3]"],#street_4',function(){
			jQuery(this).val(jQuery(this).val().trim());
			if (jQuery(this).val()=='(vazio)'){
				jQuery(this).val('');
			}
		});

		jQuery('[name=vat_id]').parent().prepend('<span class="botoes-cpf-cnpj"><span class="opcao-pf" >CPF</span> ou <span class="opcao-pj">CNPJ</span>? <small>Clique em uma das opções</small></span');
		jQuery(document).on('click','.opcao-pf',function(){
		   jQuery('[name=vat_id]').val('').attr('placeholder','Digite seu CPF');
jQuery('[name="shippingAddress.company"]').fadeOut();
			jQuery('.clicked').removeClass('clicked');
			jQuery('[name=vat_id]').addClass('forca-display-block');
			jQuery(this).addClass('clicked');
			jQuery('[name=vat_id]').unmask();
			jQuery('[name=vat_id]').mask("999.999.999-99");    
		});
		jQuery(document).on('click','.opcao-pj',function(){
		   jQuery('[name=vat_id]').val('').attr('placeholder','Digite seu CNPJ');
jQuery('[name="shippingAddress.company"]').fadeIn();
			jQuery('.clicked').removeClass('clicked');
			jQuery('[name=vat_id]').addClass('forca-display-block');
			jQuery(this).addClass('clicked');
			jQuery('[name=vat_id]').unmask();
			jQuery('[name=vat_id]').mask("99.999.999/9999-99");

		});

		jQuery(document).on('keyup','[name=postcode],[name="street[1]"],[name="street[2]"]' ,function(){
			jQuery('#shipping-method-buttons-container .button.action.continue.primary, .form-address-edit .action.save.primary').removeAttr('disabled');
			var min = 1;
			if (jQuery(this).prop('name')=='postcode'){
				min = 8;				
			}
			if(jQuery(this).val().trim().length<min){
				jQuery(this).addClass('campo-errado');
			}else{
				jQuery(this).removeClass('campo-errado');
				if (jQuery(this).prop('name')=='postcode'){
					if (window.location.pathname == '/checkout/'){
						if(jQuery('.shipping-address-item').length>0){
							enviaConteudoParaAPI(jQuery('#opc-new-shipping-address input[name=postcode]').val());	
						}else{
							enviaConteudoParaAPI(jQuery('input[name=postcode]').val());	
						}

					}else{						
						enviaConteudoParaAPI(jQuery('input[name=postcode]').val());
					}

				}

			}
		});
		jQuery(document).on('keyup','[name=vat_id]' ,function(){
			jQuery('#shipping-method-buttons-container .button.action.continue.primary, .form-address-edit .action.save.primary').removeAttr('disabled');
			jQuery(this).removeClass('campo-errado');

		});

		jQuery(document).on('mouseenter','#shipping-method-buttons-container .button.action.continue.primary, .form-address-edit .action.save.primary',function(){

		if((jQuery('.shipping-address-item').length>0) && (window.location.pathname == '/checkout/')){

		}else{

			if (!validarCPFCNPJ(jQuery('[name=vat_id]').val())){
				 jQuery('#shipping-method-buttons-container .button.action.continue.primary, .form-address-edit .action.save.primary').prop('disabled','true');
				 jQuery('[name=vat_id]').addClass('campo-errado');
				 jQuery(this).parent().addClass('add-after-missing');
			}else{
				jQuery('[name=vat_id]').removeClass('campo-errado');
				jQuery(this).parent().removeClass('add-after-missing');
			}

			if(jQuery('[name=postcode]').val().length<8){
				jQuery('#shipping-method-buttons-container .button.action.continue.primary, .form-address-edit .action.save.primary').prop('disabled','true');
				 jQuery('[name=postcode]').addClass('campo-errado');
				 jQuery(this).parent().addClass('add-after-missing');
			}else{
				jQuery('[name=postcode]').removeClass('campo-errado');
				jQuery(this).parent().removeClass('add-after-missing');
			}

			if(jQuery('[name="street[1]"],#street_2').val().length<1){
				jQuery('#shipping-method-buttons-container .button.action.continue.primary, .form-address-edit .action.save.primary').prop('disabled','true');
				 jQuery('[name="street[1]"],#street_2').addClass('campo-errado');
				 jQuery(this).parent().addClass('add-after-missing');
			}else{
				jQuery('[name="street[1]"],#street_2').removeClass('campo-errado');
				jQuery(this).parent().removeClass('add-after-missing');
			}

			if(jQuery('[name="street[2]"],#street_3').val().length<1){
				jQuery('#shipping-method-buttons-container .button.action.continue.primary, .form-address-edit .action.save.primary').prop('disabled','true');
				 jQuery('[name="street[2]"],#street_3').addClass('campo-errado');
				 jQuery(this).parent().addClass('add-after-missing');
			}else{
				jQuery('[name="street[2]"],#street_3').removeClass('campo-errado');
				jQuery(this).parent().removeClass('add-after-missing');
			}
			setTimeout(function() { jQuery('#shipping-method-buttons-container>div.primary.add-after-missing').removeClass('add-after-missing');}, 3000);

                }

		});

	}	

}

window.onload = function(e){  

jQuery('[href="https://acos.dominik.com.br/"]').attr('target','_blank')

jQuery(document).on('keyup','.field.search .control #search', function(){
	if (jQuery('#searchsuite-autocomplete').hasClass('added-aco')){
			console.log('ja tem');
	}else{

		jQuery('#searchsuite-autocomplete').addClass('added-aco').prepend('<a class="search-site-aco-pai" href="https://acos.dominik.com.br" target="_blank" ><span class="search-site-aco-filho item-menu-destaque">Buscando por itens da <strong>LINHA AÇO</strong>? <span class="cat_label Novo" rel="Novo">Clique Aqui</span></span></a>');

		jQuery('.suggest .title').text('Sugestão');
		jQuery('#searchsuite-autocomplete .no-result span').text('Sem resultados para essa busca');
	}
});

jQuery(document).on('mouseenter','.product-item-info', function(){
    jQuery(this).find('.pronta-entrega').html('<div style="font-family: Arial, Helvetica, sans-serif;text-align: center; color: white;line-height: 15px;font-weight: 400;width:250px; float:left; background-color: #18ab18; border-radius: 5px;padding: 2px;"> <div style="width:40%; float:left;font-weight: 600; font-size: 12px;"><span style="display:block; border-bottom: 1px solid white;">DOMINIK</span><span style="display:block">EXPRESS</span> </div><div style="width:30%; float:left; font-size: 12px;"><span style="display:block">Receba No</span><span style="display:block">Mesmo Dia</span> </div><div style="width:30%; float:left; font-size: 12px;"><span style="display:block">Frete</span><span style="display:block">Grátis*</span> </div></div>');
}).on('mouseleave','.product-item-info',function(){
    jQuery(this).find('.pronta-entrega').html('Pronta Entrega');
});

		(function($) {
			var pasteEventName = ($.browser.msie ? 'paste' : 'input') + ".mask";
			var iPhone = (window.orientation != undefined);

			$.mask = {

				definitions: {
					'9': "[0-9]",
					'a': "[A-Za-z]",
					'*': "[A-Za-z0-9]"
				}
			};

			$.fn.extend({

				caret: function(begin, end) {
					if (this.length == 0) return;
					if (typeof begin == 'number') {
						end = (typeof end == 'number') ? end : begin;
						return this.each(function() {
							if (this.setSelectionRange) {
								this.focus();
								this.setSelectionRange(begin, end);
							} else if (this.createTextRange) {
								var range = this.createTextRange();
								range.collapse(true);
								range.moveEnd('character', end);
								range.moveStart('character', begin);
								range.select();
							}
						});
					} else {
						if (this[0].setSelectionRange) {
							begin = this[0].selectionStart;
							end = this[0].selectionEnd;
						} else if (document.selection && document.selection.createRange) {
							var range = document.selection.createRange();
							begin = 0 - range.duplicate().moveStart('character', -100000);
							end = begin + range.text.length;
						}
						return { begin: begin, end: end };
					}
				},
				unmask: function() { return this.trigger("unmask"); },
				mask: function(mask, settings) {
					if (!mask && this.length > 0) {
						var input = $(this[0]);
						var tests = input.data("tests");
						return $.map(input.data("buffer"), function(c, i) {
							return tests[i] ? c : null;
						}).join('');
					}
					settings = $.extend({
						placeholder: "_",
						completed: null
					}, settings);

					var defs = $.mask.definitions;
					var tests = [];
					var partialPosition = mask.length;
					var firstNonMaskPos = null;
					var len = mask.length;

					$.each(mask.split(""), function(i, c) {
						if (c == '?') {
							len--;
							partialPosition = i;
						} else if (defs[c]) {
							tests.push(new RegExp(defs[c]));
							if(firstNonMaskPos==null)
								firstNonMaskPos =  tests.length - 1;
						} else {
							tests.push(null);
						}
					});

					return this.each(function() {
						var input = $(this);
						var buffer = $.map(mask.split(""), function(c, i) { if (c != '?') return defs[c] ? settings.placeholder : c });
						var ignore = false;  			
						var focusText = input.val();

						input.data("buffer", buffer).data("tests", tests);

						function seekNext(pos) {
							while (++pos <= len && !tests[pos]);
							return pos;
						};

						function shiftL(pos) {
							while (!tests[pos] && --pos >= 0);
							for (var i = pos; i < len; i++) {
								if (tests[i]) {
									buffer[i] = settings.placeholder;
									var j = seekNext(i);
									if (j < len && tests[i].test(buffer[j])) {
										buffer[i] = buffer[j];
									} else
										break;
								}
							}
							writeBuffer();
							input.caret(Math.max(firstNonMaskPos, pos));
						};

						function shiftR(pos) {
							for (var i = pos, c = settings.placeholder; i < len; i++) {
								if (tests[i]) {
									var j = seekNext(i);
									var t = buffer[i];
									buffer[i] = c;
									if (j < len && tests[j].test(t))
										c = t;
									else
										break;
								}
							}
						};

						function keydownEvent(e) {
							var pos = $(this).caret();
							var k = e.keyCode;
							ignore = (k < 16 || (k > 16 && k < 32) || (k > 32 && k < 41));

							if ((pos.begin - pos.end) != 0 && (!ignore || k == 8 || k == 46))
								clearBuffer(pos.begin, pos.end);

							if (k == 8 || k == 46 || (iPhone && k == 127)) {
								shiftL(pos.begin + (k == 46 ? 0 : -1));
								return false;
							} else if (k == 27) {
								input.val(focusText);
								input.caret(0, checkVal());
								return false;
							}
						};

						function keypressEvent(e) {
							if (ignore) {
								ignore = false;

								return (e.keyCode == 8) ? false : null;
							}
							e = e || window.event;
							var k = e.charCode || e.keyCode || e.which;
							var pos = $(this).caret();

							if (e.ctrlKey || e.altKey || e.metaKey) {
								return true;
							} else if ((k >= 32 && k <= 125) || k > 186) {
								var p = seekNext(pos.begin - 1);
								if (p < len) {
									var c = String.fromCharCode(k);
									if (tests[p].test(c)) {
										shiftR(p);
										buffer[p] = c;
										writeBuffer();
										var next = seekNext(p);
										$(this).caret(next);
										if (settings.completed && next == len)
											settings.completed.call(input);
									}
								}
							}
							return false;
						};

						function clearBuffer(start, end) {
							for (var i = start; i < end && i < len; i++) {
								if (tests[i])
									buffer[i] = settings.placeholder;
							}
						};

						function writeBuffer() { return input.val(buffer.join('')).val(); };

						function checkVal(allow) {

							var test = input.val();
							var lastMatch = -1;
							for (var i = 0, pos = 0; i < len; i++) {
								if (tests[i]) {
									buffer[i] = settings.placeholder;
									while (pos++ < test.length) {
										var c = test.charAt(pos - 1);
										if (tests[i].test(c)) {
											buffer[i] = c;
											lastMatch = i;
											break;
										}
									}
									if (pos > test.length)
										break;
								} else if (buffer[i] == test[pos] && i!=partialPosition) {
									pos++;
									lastMatch = i;
								} 
							}
							if (!allow && lastMatch + 1 < partialPosition) {
								input.val("");
								clearBuffer(0, len);
							} else if (allow || lastMatch + 1 >= partialPosition) {
								writeBuffer();
								if (!allow) input.val(input.val().substring(0, lastMatch + 1));
							}
							return (partialPosition ? i : firstNonMaskPos);
						};

						if (!input.attr("readonly"))
							input
							.one("unmask", function() {
								input
									.unbind(".mask")
									.removeData("buffer")
									.removeData("tests");
							})
							.bind("focus.mask", function() {
								focusText = input.val();
								var pos = checkVal();
								writeBuffer();
								setTimeout(function() {
									if (pos == mask.length)
										input.caret(0, pos);
									else
										input.caret(pos);
								}, 0);
							})
							.bind("blur.mask", function() {
								checkVal();
								if (input.val() != focusText)
									input.change();
							})
							.bind("keydown.mask", keydownEvent)
							.bind("keypress.mask", keypressEvent)
							.bind(pasteEventName, function() {
								setTimeout(function() { input.caret(checkVal(true)); }, 0);
							});

						checkVal(); 
					});
				}
			});
		})(jQuery);

function validarCPFCNPJ(info) {	
			if(info == ''){ return ;	}
			info = info.replace(/[^\d]+/g,'');	
			if(info == ''){ return ;	}

			if (info.length==11){		
				cpf = info;
				if (cpf.length != 11 ||  		cpf == "00000000000" || 		cpf == "11111111111" || 		cpf == "22222222222" || 		cpf == "33333333333" || 		cpf == "44444444444" || 		cpf == "55555555555" || 		cpf == "66666666666" || 		cpf == "77777777777" || 		cpf == "88888888888" || 		cpf == "99999999999") return false;		

				add = 0;	
				for (i=0; i < 9; i ++)		
					add += parseInt(cpf.charAt(i)) * (10 - i);	
					rev = 11 - (add % 11);	
					if (rev == 10 || rev == 11)		
						rev = 0;	
					if (rev != parseInt(cpf.charAt(9)))		
						return false;		

				add = 0;	
				for (i = 0; i < 10; i ++)		
					add += parseInt(cpf.charAt(i)) * (11 - i);	
				rev = 11 - (add % 11);	
				if (rev == 10 || rev == 11)	
					rev = 0;	
				if (rev != parseInt(cpf.charAt(10)))
					return false;		
				return true;   
			}else if(info.length==14){
				var cnpj = info;
				if (cnpj.length != 14)
					return false;

				if (cnpj == "00000000000000" || 
					cnpj == "11111111111111" || 
					cnpj == "22222222222222" || 
					cnpj == "33333333333333" || 
					cnpj == "44444444444444" || 
					cnpj == "55555555555555" || 
					cnpj == "66666666666666" || 
					cnpj == "77777777777777" || 
					cnpj == "88888888888888" || 
					cnpj == "99999999999999")
					return false;

				tamanho = cnpj.length - 2;
				numeros = cnpj.substring(0,tamanho);
				digitos = cnpj.substring(tamanho);
				soma = 0;
				pos = tamanho - 7;
				for (i = tamanho; i >= 1; i--) {
				  soma += numeros.charAt(tamanho - i) * pos--;
				  if (pos < 2)
						pos = 9;
				}
				resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
				if (resultado != digitos.charAt(0))
					return false;

				tamanho = tamanho + 1;
				numeros = cnpj.substring(0,tamanho);
				soma = 0;
				pos = tamanho - 7;
				for (i = tamanho; i >= 1; i--) {
				  soma += numeros.charAt(tamanho - i) * pos--;
				  if (pos < 2)
						pos = 9;
				}
				resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
				if (resultado != digitos.charAt(1))
					  return false;

				return true;
			}
			return false;
		}

		jQuery(document).on('keyup','[name=vat_id]',function(){
			if(jQuery(this).val().replace(/[^0-9]/g, '').length == 11){
		                jQuery('[name=vat_id]').unmask();
		                jQuery('[name=vat_id]').mask("999.999.999-99?9").addClass('cpf');    
	            }else if(jQuery(this).val().replace(/[^0-9]/g, '').length == 14){
        		        jQuery('[name=vat_id]').unmask();
		                jQuery('[name=vat_id]').mask("99.999.999/9999-99").addClass('cnpj');
	            }else{
		                jQuery('[name=vat_id]').unmask().val(jQuery(this).val().replace(/[^0-9]/g, '')).removeClass('cnpj').removeClass('cpf');
	            }
		});

if ((window.location.pathname == '/checkout/' || window.location.pathname == '/customer/address/new/' || window.location.pathname.includes('/customer/address/edit/')   )) {
var libera;
const myInterval = setInterval(function(){

			if (!validarCPFCNPJ(jQuery('[name=vat_id]').val())){ jQuery('[name=vat_id]').addClass('campo-errado');}else{	jQuery('[name=vat_id]').removeClass('campo-errado');}

			if(jQuery('[name=postcode]').val().length!=8){	 jQuery('[name=postcode]').addClass('campo-errado');}else{	jQuery('[name=postcode]').removeClass('campo-errado');	}			

			if(jQuery('[name="street[1]"],#street_2').val().length<1 || jQuery('[name="street[1]"],#street_2').val().length=='0'){ jQuery('[name="street[1]"],#street_2').addClass('campo-errado');}else{	
				jQuery('[name="street[1]"],#street_2').removeClass('campo-errado');}

			if(jQuery('[name="street[2]"],#street_3').val().length<1){ jQuery('[name="street[2]"],#street_3').addClass('campo-errado');}else{
				jQuery('[name="street[2]"],#street_3').removeClass('campo-errado');}

			libera = false;
			if (jQuery('.form-shipping-address [name="street[0]"]').val()!='' && 
				jQuery('.form-shipping-address [name="street[1]"]').val()!='' &&
				jQuery('.form-shipping-address [name="street[2]"]').val()!='' && 
				jQuery('.form-shipping-address [name="city"]').val()!='' &&
				jQuery('.form-shipping-address [name="region_id"]').val()!='' &&
				(	jQuery('.form-shipping-address [name="vat_id"],.form-address-edit [name=vat_id]').val().replace(/[^0-9]/g, '').length == 11 || 
					jQuery('.form-shipping-address [name="vat_id"],.form-address-edit [name=vat_id]').val().replace(/[^0-9]/g, '').length == 14) ){
						libera = true;
			}   

			if((jQuery('.shipping-address-item').length>0) && (window.location.pathname == '/checkout/')){
				 jQuery('.form.methods-shipping .button.action.continue.primary').removeAttr('disabled');
				 jQuery('.modal-footer .action-save-address').prop('disabled','true');
				 if(libera==true){
					        jQuery('.modal-footer .action-save-address').removeAttr('disabled');

				 }else{

				}

			}else{
				jQuery('.form.methods-shipping .button.action.continue.primary').prop('disabled','true');
				 if(libera==true){
					 jQuery('.form.methods-shipping .button.action.continue.primary').removeAttr('disabled');					

				 }else{

				 }
			}

						jQuery("[name=telephone]").not('.tratado')
							.addClass('tratado')
							.mask("(99) 9999-9999?9")
							.focusout(function (event) {  
								var target, phone, element;  
								target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
								phone = target.value.replace(/\D/g, '');
								element = jQuery(target);  
								element.unmask();  
								if(phone.length > 10) {  
									element.mask("(99) 99999-999?9");  
								} else {  
									element.mask("(99) 9999-9999?9");  
								}  
							});
				if (jQuery('#pagarme_creditcard-form [name="payment[cc_number]"]').length>0 && !jQuery('#pagarme_creditcard-form [name="payment[cc_number]"]').hasClass('tratado')){
					jQuery('#pagarme_creditcard-form [name="payment[cc_number]"]').addClass('tratado').hide();
					jQuery("#pagarme_creditcard-form .input-text.cc_number").after('<input type="text" class="input-text dummy-cc_number">');
						jQuery("#pagarme_creditcard-form .dummy-cc_number").mask("9999-9999-9999-9999");
						jQuery(document).on('keyup','#pagarme_creditcard-form .dummy-cc_number',function(){
							jQuery('#pagarme_creditcard-form .cc_number').val(jQuery(this).val().replace(/[^0-9]/g, ''));
							jQuery('#pagarme_creditcard-form [name="payment[cc_number]"]').change();
						});
				}

			}, 1000);
}

if (window.location.pathname == '/checkout/'){

			jQuery('body').prepend('<style>#shipping-new-address-form.fieldset.address{display:grid}#shipping-new-address-form.fieldset.address .field.street.admin__control-fields.required{order:8}#shipping-new-address-form.fieldset.address [name="shippingAddress.region_id"]{order:10}#shipping-new-address-form.fieldset.address [name="shippingAddress.city"]{order:9}#shipping-new-address-form.fieldset.address [name="shippingAddress.postcode"]{order:5}#shipping-new-address-form.fieldset.address [name="shippingAddress.telephone"]{order:2}#shipping-new-address-form.fieldset.address [name="shippingAddress.vat_id"]{order:0}#shipping-new-address-form.fieldset.address [name="shippingAddress.company"]{order:1}</style>');
		}

		if (window.location.pathname == '/customer/address/new/' || window.location.pathname.includes('/customer/address/edit/')){
			jQuery('.fieldset .field.country.required label.label').hide();
			jQuery('.field.primary label.label').text('Rua');
			jQuery('.field.street .control .nested .field.additional label:nth(0) span').text('Número');
			jQuery('.field.street .control .nested .field.additional label:nth(1) span').text('Bairro');
			jQuery('.field.street .control .nested .field.additional label:nth(2) span').text('Complemento (opcional)');
			jQuery('body').prepend('<style>fieldset.fieldset{ display:grid;}fieldset.fieldset .field.street{order:1;}fieldset.fieldset .field.taxvat{order:4;}fieldset.fieldset .field.country{order:4;}fieldset.fieldset .field.region{order:3;}fieldset.fieldset .field.city{order:2;}fieldset.fieldset .field.zip{order:0;}</style>')
		}

if (typeof $_GET['key_uri']!='undefined' &&  (window.location.pathname == '/checkout/' || window.location.pathname == '/customer/address/new/' || window.location.pathname.includes('/customer/address/edit/'))) {
var intervalo = setInterval(function(){

    if ((jQuery('[name=vat_id]').length>=2 && window.location.pathname == '/checkout/')||(jQuery('[name=vat_id]').length>=1 && window.location.pathname == '/customer/address/new/')||(jQuery('[name=vat_id]').length>=1 && window.location.pathname.includes('/customer/address/edit/'))){
        tratamento_cadastro();
        clearInterval(intervalo);
    }
},1000);

}

jQuery(document).on('keyup','#street_2,[name="street[1]"]',function(){
    jQuery(this).attr('maxlength','10');
})

if (window.location.pathname == '/checkout/onepage/success/') {
 jQuery(document).on('hover','#pagarme-link-boleto.action.tocart.primary', function(){
     jQuery('#pagarme-link-boleto.action.tocart.primary').removeClass('tocart');
 });
}

jQuery(".action-register").on("click", () => { sessionStorage.setItem('dmkClickLogin', true); })
jQuery('.customer-menu ul.links.header>li>a').on("click", () => { sessionStorage.removeItem('dmkClickLogin'); })

jQuery('a[data-whats=botao-whatsapp]').attr('href','https://api.whatsapp.com/send?phone=5548984039291&text=Ol%C3%A1,%20'+'estou%20na%20p%C3%A1gina%20'+window.location+'%20e%20preciso%20de%20ajuda!');

jQuery('div.magicmenu .nav-desktop.sticker').append('<li class="level0 category-item level-top cat nav-3" style="position: absolute;right: 12px;top: 9px;width: 240px;z-index: 0;">'+jQuery('.header-logo').html()+'</li>');

jQuery('.message-error.error.message div:contains(cupom)').parent().css('display','block')

};