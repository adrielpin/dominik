<?php

namespace Dominik\InstallmentPrice\Pricing\Render;

use Dominik\InstallmentPrice\Block\InstallmentPrice;
use Magento\Catalog\Model\Product\Pricing\Renderer\SalableResolverInterface;
use Magento\Catalog\Pricing\Price\MinimalPriceCalculatorInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\Render\RendererPool;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Layout;

class FinalPriceBox extends \Magento\Catalog\Pricing\Render\FinalPriceBox
{
    private ScopeConfigInterface $config;
    private Layout $layout;

    /**
     * @param ScopeConfigInterface $config
     * @param Layout $layout
     * @param Context $context
     * @param SaleableInterface $saleableItem
     * @param PriceInterface $price
     * @param RendererPool $rendererPool
     * @param array $data
     * @param SalableResolverInterface|null $salableResolver
     * @param MinimalPriceCalculatorInterface|null $minimalPriceCalculator
     */
    public function __construct(
        ScopeConfigInterface $config,
        Layout $layout,
        Context $context,
        SaleableInterface $saleableItem,
        PriceInterface $price,
        RendererPool $rendererPool,
        array $data = [],
        SalableResolverInterface $salableResolver = null,
        MinimalPriceCalculatorInterface $minimalPriceCalculator = null)
    {
        parent::__construct($context, $saleableItem, $price, $rendererPool, $data, $salableResolver, $minimalPriceCalculator);
        $this->config = $config;
        $this->layout = $layout;
    }

    protected function wrapResult($html)
    {
        $result = parent::wrapResult($html);
        $result .= $this->layout
            ->createBlock(InstallmentPrice::class)
            ->setTemplate('Dominik_InstallmentPrice::product/price/installments.phtml')
            ->setIsProductListing(true)
            ->setProduct($this->saleableItem)
            ->toHtml();
        return $result;
    }
}
