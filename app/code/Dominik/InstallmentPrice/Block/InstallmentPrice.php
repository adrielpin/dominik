<?php

namespace Dominik\InstallmentPrice\Block;

use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Element\Template;

class InstallmentPrice extends Template
{
    /**
     * @var Json
     */
    protected Json $_jsonEncoder;

    /**
     * @var FormatInterface
     */
    protected FormatInterface $_localeFormat;

    /**
     * @param Template\Context $context
     * @param Json $jsonEncoder
     * @param FormatInterface $localeFormat
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Json             $jsonEncoder,
        FormatInterface  $localeFormat,
        array            $data = []
    )
    {
        $this->_jsonEncoder = $jsonEncoder;
        $this->_localeFormat = $localeFormat;
        parent::__construct($context, $data);
    }

    /**
     * @return float
     */
    public function getInterestInitialPercentage(): float
    {
        return (float)$this->_scopeConfig->getValue('payment/pagarme_creditcard/installments_interest_rate_initial');
    }

    /**
     * @return float
     */
    public function getInterestIncrementalPercentage(): float
    {
        return (float)$this->_scopeConfig->getValue('payment/pagarme_creditcard/installments_interest_rate_incremental');
    }

    /**
     * @return int
     */
    public function getInstallmentNumber(): int
    {
        return (int)$this->_scopeConfig->getValue('payment/pagarme_creditcard/installments_number');
    }

    /**
     * @return int
     */
    public function getInstallmentMinQuota(): int
    {
        return (int)$this->_scopeConfig->getValue('payment/pagarme_creditcard/installment_min_amount');
    }

    /**
     * @return int
     */
    public function getInstallmentNumberWithoutInterest(): int
    {
        return (int)$this->_scopeConfig->getValue('payment/pagarme_creditcard/installments_max_without_interest');
    }

    /**
     * @return bool
     */
    public function getIsEnabledOnProductList(): bool
    {
        return $this->_scopeConfig->isSetFlag('catalog/installment/list_is_enabled');
    }

    /**
     * @return bool
     */
    public function getIsEnabledOnProductPage(): bool
    {
        return $this->_scopeConfig->isSetFlag('catalog/installment/product_is_enabled');
    }

    /**
     * @return bool
     */
    public function getShowOnlyTheLastOneInProductList(): bool
    {
        return $this->_scopeConfig->isSetFlag('catalog/installment/list_show_only_last');
    }

    /**
     * @return string
     */
    public function getShowOnlyTheLastOneInProductPage(): string
    {
        return $this->_scopeConfig->getValue('catalog/installment/product_show_only_last');
    }

    /**
     * @return string
     */
    public function getPriceFormatJsonConfig(): string
    {
        return $this->_jsonEncoder->serialize($this->_localeFormat->getPriceFormat());
    }
}
