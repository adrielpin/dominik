define([
    'jquery',
    'mage/template',
    'underscore',
    'Magento_Catalog/js/price-utils',
    'jquery/ui'
], function ($, mageTemplate, _, priceUtils) {
    'use strict';

    $.widget('mage.installmentPriceBox', {
        options: {
            interest: {
                on: false,
                initialPercentage: 0,
                incrementalPercentage: 0,
                priceQuote: 0
            },
            installment: {
                number: 0,
                numberWithoutInterest: 0,
                priceTemplate: '#installment-price-template',
                show: {
                    onlyLastOne: false,
                    start: 1,
                    stop: 0
                }
            },
            finalPriceSelector: '[data-role=priceBox].price-final_price',
            finalPrice: 0,
            installments: [],
            priceFormat: {},
            perfectNumber: 0    //Minor installment times
        },

        _init: function () {
            if (!this.options.installment.number) {
                return;
            }

            if (this.options.installment.show.onlyLastOne) {
                this.options.installment.show.start = this.options.installment.number;
                this.options.installment.show.stop = this.options.installment.number + 1;
            } else {
                this.options.installment.show.stop = this.options.installment.show.stop || this.options.installment.number + 1;
            }

            //this.options.finalPrice = $(this.options.finalPriceSelector).data('magePriceBox')
            //    ? $(this.options.finalPriceSelector).data('magePriceBox').options.prices.finalPrice.amount
            //    : $(this.options.finalPriceSelector).find('.price-final_price .price-wrapper').data('priceAmount');

            if (this.options.finalPrice) {
                this._reloadInstallments();
                this._redrawInstallments();

                $(this.options.finalPriceSelector).on('updatePrice', this._updatePrice.bind(this));
            }
        },

        _updatePrice: function (event) {
            // this.options.finalPrice = $(event.target).data('magePriceBox').cache.displayPrices.finalPrice.final;
            this._reloadInstallments();
            this._redrawInstallments();
        },

        _calcInterestRate: function (qty) {
            var interestRate = 0,
                diff = qty - this.options.installment.numberWithoutInterest;
            if (diff >= 1) {
                interestRate = ((diff - 1) * this.options.interest.incrementalPercentage) + this.options.interest.initialPercentage;
            }
            return interestRate;
        },

        _calcInstallmentAmount: function (qty, interestRate) {
            return (this.options.finalPrice * (1 + (interestRate / 100))) / qty;
        },

        _reloadInstallments: function () {
            this.options.installments = [];

            _.each(_.range(this.options.installment.show.start, this.options.installment.show.stop), function (qty) {
                var total = this.options.finalPrice,
                    oldTotal = total,
                    price = parseFloat(total / qty),
                    interestAmount = 0,
                    interestRate = 0,
                    hasInterest = false;

                if (this.options.interest.on && (qty > this.options.installment.numberWithoutInterest)) {
                    hasInterest = true;
                    if (this.options.perfectNumber) {
                        interestRate = this._calcInterestRate(this.options.perfectNumber);
                    } else {
                        interestRate = this._calcInterestRate(qty);
                    }
                    price = this._calcInstallmentAmount(qty, interestRate);
                    total = price * qty;
                    interestAmount = total - oldTotal;
                }

                while (price < this.options.interest.priceQuote) {
                    if (qty == 1) {
                        break;
                    } else {
                        qty--;
                        price = total / qty;
                        this.options.perfectNumber = qty;
                    }
                }
                this.options.installments.push({
                    qty: qty,
                    price: price,
                    total: total,
                    hasInterest: hasInterest,
                    interestAmount: interestAmount,
                    interestRate: interestRate,
                    formatted: {
                        price: priceUtils.formatPrice(price, this.options.priceFormat),
                        total: priceUtils.formatPrice(total, this.options.priceFormat),
                        interestAmount: priceUtils.formatPrice(interestAmount, this.options.priceFormat)
                    }
                });
            }, this);
        },

        _redrawInstallments: function () {
			
            var html = '',
				
                installmentPriceTemplate = mageTemplate(this.options.installment.priceTemplate);

            _.each(this.options.installments, function (installment) {
				if (installment.qty==1){
					installment.price = installment.price - installment.interestAmount;
					installment.total = installment.price;
					installment.formatted.price = ''+String(installment.price.toFixed(2)).replace('.',',');
					installment.formatted.total = ''+String(installment.price.toFixed(2)).replace('.',',');
					installment.formatted.interestAmount = 0;
				}
                html += installmentPriceTemplate({ installment: installment });
            });

            this.element.html(html);
        }

    });

    return $.mage.installmentPriceBox;
});
