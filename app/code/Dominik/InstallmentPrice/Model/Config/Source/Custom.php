<?php

namespace Dominik\InstallmentPrice\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Custom implements OptionSourceInterface
{
    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            ['value' => '0', 'label' => 'all installments'],
            ['value' => '1', 'label' => 'only last installment'],
            ['value' => '2', 'label' => 'both modes']
        ];
    }
}
