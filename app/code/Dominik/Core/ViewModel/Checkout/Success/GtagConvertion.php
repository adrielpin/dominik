<?php

namespace Dominik\Core\ViewModel\Checkout\Success;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory as OrderModel;
use Magento\Framework\Serialize\Serializer\Json;

class GtagConvertion implements ArgumentInterface
{
    public const CURRENCY = 'BRL';

    /**
     * @var Json
     */
    private Json $json;

    /**
     * @var OrderModel
     */
    private OrderModel $orderModel;

    /**
     * @var Order
     */
    private Order $orderInfo;


    /**
     * @param OrderModel $orderModel
     * @param Json $json
     */
    public function __construct(
        OrderModel $orderModel,
        Json $json

    ){
        $this->orderModel = $orderModel;
        $this->json = $json;
    }


    /**
     * Load order information
     *
     * @param int $orderId
     *
     * @return void
     */
    public function loadOrderInformation(int $orderId): void
    {
        $orderModelInstance = $this->orderModel->create();
        $this->orderInfo = $orderModelInstance->loadByIncrementId($orderId);
    }


    /**
     * Return a payload to success page
     *
     * @return string
     */
    public function getPayload() : string
    {
        $productsSale = $this->orderInfo->getAllItems();

        $payload = [
         "transaction_id" => $this->orderInfo->getIncrementId(),
            "affiliation" => "Google online store",
                  "value" => $this->orderInfo->getGrandTotal(),
               "currency" => self::CURRENCY,
                    "tax" => $this->orderInfo->getBaseTaxAmount(),
               "shipping" => $this->orderInfo->getShippingTaxAmount(),
        ];
        $items = [];
        foreach ($productsSale as $key => $item) {
            $product = $item->getProduct();

            $items[] = [
                    "id"    => $product->getSku(),
                    "name"  => $product->getName(),
                "list_name" => "Search Results",
                    "brand" => $product->getAttributeText('manufacturer'),
                 "category" => $product->getCategoryCollection()->getFirstItem()->getPath(),
                  "variant" => $product->getAttributeText('color'),
                "list_position" => $key+1,
                 "quantity" => round($item->getQtyOrdered(), 2),
                    "price" => round($item->getPrice(), 2)
            ];
        }
        $payload['items'] = $items;

        return addslashes($this->json->serialize($payload));
    }
}
