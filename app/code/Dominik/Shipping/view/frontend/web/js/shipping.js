define(["jquery", "domReady!"], ($) => {
    "use strict";
    return (config) => {
        var s = $("#btn-estimate-shipping");
        let postCode = $("#dominik-postcode");
        postCode.on('keyup', function(e) {
            s.click();
        });
        s.on("click", function() {
            let cep = postCode.val();
            let divResults = $("#shipping-estimate-results");
            let qty = $("#qty").val();
            divResults.slideUp();
            if (void 0 !== cep && $.isNumeric(cep) && 8 === cep.length) {
                postCode.removeClass("has-error");
                $('#btn-estimate-shipping').addClass('loading-estimate-icon');

                let dataField = "cep=" + cep + "&product=" + config.product_id + "&qty=" + qty + "&form_key=" + config.form_key;
                if (config.product_type === 'configurable') {
                    let idSelectedProduct = $('input[name="selected_configurable_option"]').val();
                    dataField += '&product_selected=' + idSelectedProduct;
                }
                $.ajax({
                    type: "post",
                    url: BASE_URL + "dominikshipping/product/estimate/",
                    data: dataField,
                    showLoader: false,
                    complete: (i) => {
                        $('#btn-estimate-shipping').removeClass('loading-estimate-icon');
                    },
                    success: (i) => {
                        divResults.html("");
                        if (i.error) {
                            divResults.html("<li>" + i.error.message + "</li>").slideDown();
                        } else {
                            for (let prop in i) {
                                if (i[prop].length > 0) {
									console.log(i[prop][0].carrier);
                                    let n = $('<li><span class="title '+i[prop][0].carrier+'">' + i[prop][0].carrier+ "</span></li>");
                                    var a = $("<ul></ul>");
                                    i[prop].forEach((obj) => {
                                        let j = $('<li><span class="title '+obj.title+'">' + obj.title + "</span>" + obj.price + "</li>");
                                        if (obj.message != "") {
                                            j.append("- " + obj.message);
                                        }
                                        a.append(j);
                                    });
                                    n.append(a);
                                    divResults.append(n).slideDown();
                                }
                            }
							if (jQuery('.Entrega.Dominik').length>0){
										jQuery('#shipping-estimate-results>li:nth-child(2)').hide();
									}else{
										jQuery('#shipping-estimate-results>li:nth-child(2)').show();
									}
                        }
                    }
                });
            } else {
                postCode.focus().addClass("has-error");
            }
        });
    }
});
