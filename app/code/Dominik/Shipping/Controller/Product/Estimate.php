<?php

namespace Dominik\Shipping\Controller\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Quote\Model\Quote\Address\Rate;
use Magento\Quote\Model\QuoteFactory;

class Estimate implements HttpPostActionInterface
{
    protected JsonFactory $jsonFactory;
    protected ProductRepositoryInterface $product_repository;
    protected QuoteFactory $quote;
    protected Data $pricingHelper;

    /**
     * Estimate constructor.
     * @param ProductRepositoryInterface $product_repository
     * @param JsonFactory $jsonFactory
     * @param QuoteFactory $quote
     * @param Data $pricingHelper
     */
    public function __construct(
        ProductRepositoryInterface $product_repository,
        JsonFactory                $jsonFactory,
        QuoteFactory               $quote,
        Data                       $pricingHelper
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->product_repository = $product_repository;
        $this->quote = $quote;
        $this->pricingHelper = $pricingHelper;
    }

    /**
     * @return Json
     */
    public function execute(): Json
    {
        $_params = $_POST;
        $response = [];

        if (
            empty($_params) ||
            !isset($_params['cep']) ||
            $_params['cep'] == ""
        ) {
            $response['error']['message'] = __('Postcode not informed');
        } else if (
            !isset($_params['product']) ||
            $_params['product'] == "" ||
            $_params['product'] == 0 ||
            !is_numeric($_params['product'])
        ) {
            $response['error']['message'] = __('Amount reported is invalid');
        }

        if (!isset($response['error'])) {
            if (
                !isset($_params['qty']) ||
                $_params['qty'] == "" ||
                $_params['qty'] == 0 ||
                !is_numeric($_params['qty'])
            ) {
                $qty = 1;
            } else {
                $qty = $_params['qty'];
            }

            try {
                $_product = $this->product_repository->getById($_params['product']);

                $quote = $this->quote->create();
                $quote->addProduct($_product, $qty);
                $quote->getShippingAddress()->setCountryId('BR');
                $quote->getShippingAddress()->setPostcode($_params['cep']);
                $quote->getShippingAddress()->setCollectShippingRates(true);
                $quote->getShippingAddress()->collectShippingRates();
                $rates = $quote->getShippingAddress()->getShippingRatesCollection();
                if (count($rates) > 0) {
                    $shipping_methods = [];

                    /** @var Rate $rate */
                    foreach ($rates as $rate) {
                        $_message = !$rate->getErrorMessage() ? "" : $rate->getErrorMessage();
                        $shipping_methods[$rate->getCarrier()][] = [
                            'carrier' => $rate->getCarrierTitle(),
                            'title' => $rate->getMethodTitle(),
                            'price' => $this->pricingHelper->currency($rate->getPrice()),
                            'message' => $_message,
                        ];
                    }
                    $response = $shipping_methods;
                } else {
                    $response['error']['message'] = __('There is no shipping method available at this time.');
                }
            } catch (\Exception $e) {
                $response['error']['message'] = $e->getMessage();
                return $this->jsonFactory->create($response);
            }
        }
        $json = $this->jsonFactory->create();
        $json->setJsonData(json_encode($response, JSON_HEX_TAG));
        return $json;
    }
}
