<?php
namespace Dominik\Shipping\Block;

use Magento\Catalog\Model\Product;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Block\Product\View;

class Estimate extends Template
{
    protected View $_product_view;
    protected FormKey $formKey;

    /**
     * Estimate constructor.
     * @param FormKey $formKey
     * @param View $_product_view
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        FormKey $formKey,
        View $_product_view,
        Context $context,
        array $data = []
    ) {
        $this->formKey = $formKey;
        $this->_product_view = $_product_view;
        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProductInfo() {
        return $this->_product_view->getProduct();
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getFormKey(){
        return $this->formKey->getFormKey();
    }
}
