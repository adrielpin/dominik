<?php

namespace Magefan\WebP\Plugin\Magento\PageBuilder\Model\Filter;

use Magefan\WebP\Model\Config;
use Magefan\WebP\Model\Parser\PageBuilder as Parser;

class Template
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @param Config $config
     * @param Parser $parser
     */
    public function __construct(
        Config $config,
        Parser $parser
    ) {
        $this->config = $config;
        $this->parser = $parser;
    }

    /**
     * @param $subject
     * @param $result
     * @return mixed|string|string[]
     */
    public function afterFilter($subject, $result)
    {
        return ($this->config->isEnabled() && $this->config->convertImagesInsertedViaPageBuilder()) ?
            $this->parser->processBackgroundImages($result) :  $result  ;
    }

    /**
     * @param $subject
     * @param $result
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterAroundAfterFilter($subject, $result): string
    {
        if (class_exists(\Hyva\Theme\Service\CurrentTheme::class)) {
            $theme = \Magento\Framework\App\ObjectManager::getInstance()->get(\Hyva\Theme\Service\CurrentTheme::class);

            if ($theme->isHyva() &&
                is_string($result) &&
                $this->config->isEnabled() &&
                $this->config->convertImagesInsertedViaPageBuilder())
            {
                $hyvaRegex = '/\[data-image-id="([\w\d]+)"\]\s{background-image: url\((.*?)\);}/mi';

                $result = $this->parser->processBackgroundImages($result, $hyvaRegex);
            }
        }

        return $result;
    }
}
