<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */
declare(strict_types=1);

namespace Magefan\WebP\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magefan\WebP\Model\Config;

class WebP implements ArgumentInterface
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Config
     */
    private $config;

    /**
     * WebpP constructor.
     * @param StoreManagerInterface $storeManager
     * @param Config $config
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        Config $config = null
    ) {
        $this->storeManager = $storeManager;
        $this->config = $config ?: \Magento\Framework\App\ObjectManager::getInstance()->get(Config::class);
    }

    /**
     * @return array
     */
    public function getStoreUrls():array
    {
        $store = $this->storeManager->getStore();

        return [
            'base' => $store->getBaseUrl(UrlInterface::URL_TYPE_WEB),
            'media' => $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA),
            'static' => preg_replace('/version\d{10}\//i', '', $store->getBaseUrl(UrlInterface::URL_TYPE_STATIC))
        ];
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }
}
