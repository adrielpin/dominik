<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

declare(strict_types = 1);

namespace Magefan\WebP\Block\Adminhtml\System\Config\Form;

use Magefan\WebP\Model\Config\Source\CreationOptions;
use Magefan\WebP\Model\ResourceModel\Image\CollectionFactory as ImageQueueCollectionFactory;
use Magento\Backend\Block\Template\Context;
use Magefan\WebP\Model\Config;

/**
 * Class Button. Render convert button to view.
 */
class ProgressBar extends \Magento\Config\Block\System\Config\Form\Field
{
    const PROGRESS_BAR_TEMPLATE = 'system/config/progress_bar.phtml';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ImageQueueCollectionFactory
     */
    private $imageCollectionFactory;

    /**
     * @param Context $context
     * @param ImageQueueCollectionFactory $imageCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        ImageQueueCollectionFactory $imageCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
        $this->imageCollectionFactory = $imageCollectionFactory;
    }


    /**
     * Set template to itself
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(static::PROGRESS_BAR_TEMPLATE);
        }
        return $this;
    }
    /**
     * Render button
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        // Remove scope label
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        if ($this->config->isEnabled()) {
            if (in_array($this->config->getGenerationOption(), [CreationOptions::CRON, CreationOptions::PAGE_LOAD_AND_CRON])) {
                return $this->_toHtml();
            }
        }

        return '';
    }

    /**
     * @return array
     */
    public function getWebPGenerationInfo(): array
    {
        $result = [
            'count_of_images_to_convert' => count($this->imageCollectionFactory->create())
        ];

        $data = $this->config->getDadaByConvertedImagesForLast24Hours();

        if (count($data) > Config::COUNT_OF_RECORDS_PER_HOUR) {

            // if extension converted per last 5 cron jobs (25 min) more than 2000 images - all images converted. Images skipped or can't be converted
            if (array_sum(array_slice($data, -5)) > 2000) {
                return ['done' => true];
            }

            $result['count_of_images_converted_per_last_24_hours'] = array_sum($data);
            $result['avg_count_of_images_converted_per_hour'] =  array_sum(array_slice($data, -Config::COUNT_OF_RECORDS_PER_HOUR));
            $result['hours_to_convert_all_images'] = [
                'hours' => ceil($result['count_of_images_to_convert']/$result['avg_count_of_images_converted_per_hour']),
                'days' => ceil($result['count_of_images_to_convert']/$result['avg_count_of_images_converted_per_hour']/24)
            ];
        } else {
            $result['no_enough_data'] = __('Statistics will be available in one hour!');
        }

        return $result;
    }
}
