var config = {
    config: {
        mixins: {
            'mage/gallery/gallery': {
                'Magefan_WebP/js/gallery/gallery-mixin': true
            },
            'Magento_Swatches/js/swatch-renderer': {
                'Magefan_WebP/js/swatch-renderer-mixin': true
            },
            'Aimes_Notorama/js/notorama': {
                'Magefan_WebP/js/gallery/notorama-mixin': true
            },
            'Xumulus_FastGalleryLoad/js/gallery/custom_gallery': {
                'Magefan_WebP/js/gallery/gallery-mixin': true
            },
            'Codazon_ProductFilter/js/product-gallery': {
                'Magefan_WebP/js/codazon/product-gallery-mixin': true
            },
            'js/theme-widgets': {
                'Magefan_WebP/js/codazon/theme-widgets-mixin': true
            },
            'Magento_Catalog/js/product/list/columns/image' : {
                'Magefan_WebP/js/product/list/columns/image-mixin': true
            }
        }
    }
};
